<?php

class Config {
	// default sitename
	public $sitename = 'AMELIA';
	// default site fullname
	public $sitefullname = 'Aplikasi Manajemen Data Personalia';
	// site protocol (http/https)
	public $protocol = 'http';
	// copyright
	public $copyright = 'Vicky Budiman (32140004)';
	// site version
	public $version = '1.0.0';
	// logo
	public $logo = '';
	// logo mini
	public $logomini = 'AML';
	// logo image
	public $logoimg = '';
	// favicon
	public $favicon = '';
	// default error message E_ALL
	public $reporting = E_ALL;
	// database server
	public $dbserver = 'localhost';
	// database user
	public $dbuser = 'root';
	// database pass
	public $dbpass = '';
	// database name
	public $dbname = 'amelia';
	// database port
    public $dbport = null;
	// SMTP Debug
	public $SMTPDebug  = 1;
	// SMTP Auth
	public $SMTPAuth   = true;
	// SMTP Secure
	public $SMTPSecure = "ssl";
	// SMTP Host
	public $SMTPHost = "";
	// SMTP Port
	public $SMTPPort = 0;
	// SMTP Username
	public $SMTPUsername   = "";
	// SMTP Password
	public $SMTPPassword   = "";
	// SMTP From Name
	public $SMTPFromName = "";
	// SMTP From Email
	public $SMTPFromEmail = "";
	// Default userlevel for new user (check database)
	public $userlevel = 1;
	// Feature forgot password
	public $forgotpass = false;
	// Color Skin Template
	public $skin = "blue";
	// Path for save uploaded files
	public $path = "uploads/files";
	// Allowed extension files to be uploaded
	public $allowed = array("png", "jpeg", "jpg", "gif", "bmp", "pdf", "doc", "docx", "txt", "csv", "xls", "xlsx");
	// Extension for encrypted file
	public $ext = ".vic";
	// Maximum Size for upload file (MB)
	public $maxsize = 5;
}

?>